//
//  TTViewController.m
//  TTTest
//
//  Created by Diego Peinador on 06/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import "TTViewController.h"
#import "DPTopTabsViewController.h"
#import "TTSimpleViewController.h"

@interface TTViewController (){
    UIPopoverController *_popover;
    DPTopTabsViewController *_toptabsVC;
    NSUInteger _colorIndex;
    BOOL _usingImages;
}

@end

@implementation TTViewController
@synthesize testView=_testView;
@synthesize vcId = _vcId;
@synthesize overlapLabel = _overlapLabel;
@synthesize heightLabel = _heightLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    TTSimpleViewController *sVC1 = [[TTSimpleViewController alloc] init];
    sVC1.toptapItem = [DPTopTabBarItem alloc];
    sVC1.toptapItem.title = @"1";
    sVC1.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    TTSimpleViewController *sVC2 = [[TTSimpleViewController alloc] init];
    sVC2.toptapItem = [DPTopTabBarItem alloc];
    sVC2.toptapItem.title = @"2";
    sVC2.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    TTSimpleViewController *sVC3 = [[TTSimpleViewController alloc] init];
    sVC3.toptapItem = [DPTopTabBarItem alloc];
    sVC3.toptapItem.title = @"3";
    sVC3.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];

    _toptabsVC = [[DPTopTabsViewController alloc] initWithViewControllers:sVC1,sVC2,sVC3, nil];
    _toptabsVC.titlesBackgroundColor = [UIColor lightGrayColor];
    [self addChildViewController:_toptabsVC];
    _toptabsVC.view.frame = _testView.bounds;
    [_testView addSubview:_toptabsVC.view];
}

- (void)viewDidUnload
{
    [self setTestView:nil];
    [self setVcId:nil];
    [self setOverlapLabel:nil];
    [self setHeightLabel:nil];
    [self setSizeSlider:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(CGFloat)random{
    return arc4random()%100/100.0;
}

- (IBAction)addVC:(id)sender {
    TTSimpleViewController *sVC = [[TTSimpleViewController alloc] init];
    sVC.toptapItem = [DPTopTabBarItem alloc];
    sVC.toptapItem.title = _vcId.text;
    sVC.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    [_toptabsVC addViewController:sVC animated:YES];
}

- (IBAction)removeVC:(id)sender {
    NSString *text = _vcId.text;
    NSInteger found=-1;
    for (int i=0; i<[_toptabsVC viewControllersCount]; i++) {
        if ([[_toptabsVC viewControllerAtIndex:i].toptapItem.title isEqualToString:text]) {
            found = i;
            break;
        }
    }
    if (found!=-1) {
        [_toptabsVC removeViewControllerAtIndex:found animated:YES];
    }
}

- (IBAction)updateTitleHeight:(id)sender {
    _toptabsVC.titleViewSize = ((UISlider*)sender).value;
    self.heightLabel.text = [NSString stringWithFormat:@"%.0f",_toptabsVC.titleViewSize];
    self.overlapLabel.text = [NSString stringWithFormat:@"%.0f",_toptabsVC.titleOverlap];
}

- (IBAction)changeAligment:(id)sender {
    _toptabsVC.textAlignment = (_toptabsVC.textAlignment+1)%3;
}

- (IBAction)changeColor:(id)sender {
    _colorIndex = (++_colorIndex)%6;
    switch (_colorIndex) {
        case 1:
            _toptabsVC.tint=[UIColor redColor];
            break;
        case 2:
            _toptabsVC.tint=[UIColor brownColor];
            break;
        case 3:
            _toptabsVC.tint=[UIColor orangeColor];
            break;
        case 4:
            _toptabsVC.tint=[UIColor purpleColor];
            break;
        case 5:
            _toptabsVC.tint=[UIColor magentaColor];
            break;
            
        default:
            _toptabsVC.tint=[UIColor blueColor];
            break;
    }
}

-(IBAction)changeStyle:(id)sender{
    if (!_usingImages) { // example images are not ready for vertical orientations
        _toptabsVC.tabStyle=(_toptabsVC.tabStyle+1)%4;
        switch (_toptabsVC.tabStyle) {
            case DPTopTabsStyleNormal:
                _sizeSlider.minimumValue=22;
                break;
            case DPTopTabsStyleLikeTabBar:
                _sizeSlider.minimumValue=44;
                break;
            default:
                _sizeSlider.minimumValue=64;
                break;
        }
        _sizeSlider.maximumValue=3*_sizeSlider.minimumValue;
        self.heightLabel.text = [NSString stringWithFormat:@"%.0f",_toptabsVC.titleViewSize];
    }
}

- (IBAction)updateTitleOverlap:(id)sender {
    _toptabsVC.titleOverlap = ((UISlider*)sender).value;
    self.overlapLabel.text = [NSString stringWithFormat:@"%.0f",_toptabsVC.titleOverlap];
}

- (IBAction)useImages:(id)sender {
    if (_toptabsVC.tabStyle==DPTopTabsStyleNormal) {
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 19, 0, 19);
        [_toptabsVC setBackgroundImage:[[UIImage imageNamed:@"normal"] resizableImageWithCapInsets:edgeInsets] forState:UIControlStateNormal];
        [_toptabsVC setBackgroundImage:[[UIImage imageNamed:@"selected"] resizableImageWithCapInsets:edgeInsets] forState:UIControlStateSelected];
        [_toptabsVC setDividerImage:[UIImage imageNamed:@"N-N"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal];
        [_toptabsVC setDividerImage:[UIImage imageNamed:@"N-S"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected];
        [_toptabsVC setDividerImage:[UIImage imageNamed:@"S-N"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal];
        _usingImages=YES;
    }
}

- (IBAction)reset:(id)sender {
    [_toptabsVC.view removeFromSuperview];
    _toptabsVC = nil;
    
    _usingImages=NO;

    TTSimpleViewController *sVC1 = [[TTSimpleViewController alloc] init];
    sVC1.toptapItem = [DPTopTabBarItem alloc];
    sVC1.toptapItem.title = @"1";
    sVC1.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    TTSimpleViewController *sVC2 = [[TTSimpleViewController alloc] init];
    sVC2.toptapItem = [DPTopTabBarItem alloc];
    sVC2.toptapItem.title = @"2";
    sVC2.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    TTSimpleViewController *sVC3 = [[TTSimpleViewController alloc] init];
    sVC3.toptapItem = [DPTopTabBarItem alloc];
    sVC3.toptapItem.title = @"3";
    sVC3.fondo = [UIColor colorWithRed:[self random] green:[self random] blue:[self random] alpha:1];
    
    _toptabsVC = [[DPTopTabsViewController alloc] initWithViewControllers:sVC1,sVC2,sVC3, nil];
    _toptabsVC.titlesBackgroundColor = [UIColor lightGrayColor];
    [self addChildViewController:_toptabsVC];
    _toptabsVC.view.frame = _testView.bounds;
    [_testView addSubview:_toptabsVC.view];
}

- (IBAction)changeTextColor:(id)sender {
    UIColor *newTextColor;
    _colorIndex = (++_colorIndex)%6;
    switch (_colorIndex) {
        case 1:
            newTextColor=[UIColor magentaColor];
            break;
        case 2:
            newTextColor=[UIColor cyanColor];
            break;
        case 3:
            newTextColor=[UIColor lightGrayColor];
            break;
        case 4:
            newTextColor=[UIColor darkGrayColor];
            break;
        case 5:
            newTextColor=[UIColor blackColor];
            break;
            
        default:
            newTextColor=[UIColor whiteColor];
            break;
    }
    [_toptabsVC setTitleTextAttributes:@{UITextAttributeTextColor : newTextColor} forState:UIControlStateNormal];

}

@end
