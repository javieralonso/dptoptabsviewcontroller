//
//  DPTopTabsViewController.h
//
//  Created by Diego Peinador on 20/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TOPTABS_TITLE_LINE_WIDTH 1  // Nice on Retina, use 2 otherwise
#define TOPTABS_TITLE_MIN_WIDTH 40  // Minimun size for title label
#define TOPTABS_TITLE_TOP_MARGIN 5  // Top and bottom margins
#define TOPTABS_TITLE_IMAGE_SIDE 30 // Height and width of the image, if tab height is changed only the margins will change, this stays the same
#define TOPTABS_ANIMATION_DURATION 0.3

typedef NS_ENUM(NSInteger, DPTopTabsStyle) {
    DPTopTabsStyleNormal = 0, // default. icon on right (if available)
    DPTopTabsStyleLikeTabBar, // icon on top, text centered aligned
    DPTopTabsStyleLeftTabBar,
    DPTopTabsStyleRightTabBar,
};

@protocol DPTopTabsViewControllerDelegate;

@interface DPTopTabsViewController : UIViewController

@property (nonatomic, weak) id<DPTopTabsViewControllerDelegate> delegate;
@property (nonatomic, strong) UIColor *titlesBackgroundColor; // Use this to match surrounding color, i.e.: [UIColor blackColor]
@property (nonatomic, strong) UIColor *tint; // Default is [UIColor blueColor]
                                             // Used for line stroke as is, for selected title with saturation * 0.55
                                             // Used for not selected titles with saturation * 0.25
@property (nonatomic, strong) UIColor *selectedColor;    // Ignore tint and use this for selected title
@property (nonatomic, strong) UIColor *notSelectedColor; // Ignore tint and use this for not selected titles
@property (nonatomic) CGFloat titleViewSize;  // default is 2 * TOPTABS_TITLE_TOP_MARGIN + TOPTABS_TITLE_IMAGE_SIDE
                                                // used as height when tabStyle = DPTopTabsStyleNormal | tabStyle = DPTopTabsStyleLikeTabBar
                                                // used as width when tabStyle = DPTopTabsStyleLeftTabBar | tabStyle = DPTopTabsStyleRightTabBar
@property (nonatomic) CGFloat titleOverlap; // default is _titleViewHeight/2
@property (nonatomic) UITextAlignment textAlignment; // default is UITextAlignmentLeft
@property (nonatomic, readonly) NSInteger activeTab; // index of the active tab
@property (nonatomic) DPTopTabsStyle tabStyle;

// If you setup the images, height will be fixed acording to image height, and overlap will be 0
// Valid UIControlState are: UIControlStateNormal and UIControlStateSelected only
-(void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state;
-(void)setDividerImage:(UIImage *)dividerImage forLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState;

// Use like -[UISegmentedControl setTitleTextAttributes:forState:]
// Defaults:
//   textColor=[UIColor whiteColor];
//   shadowColor=[UIColor darkGrayColor];
//   shadowOffset=CGSizeMake(1, 1);
//   font=[UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
-(void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state;


-(id)initWithViewControllers:(UIViewController *)firstVC, ... NS_REQUIRES_NIL_TERMINATION;

-(int)viewControllersCount;
-(void)addViewController:(UIViewController *)newVC animated:(BOOL)animated;
-(void)insertViewController:(UIViewController *)newVC atIndex:(int)index animated:(BOOL)animated;
-(void)removeViewControllerAtIndex:(int)index animated:(BOOL)animated;
-(UIViewController *)viewControllerAtIndex:(int)index;
-(void)activateViewControllerAtIndex:(int)index animated:(BOOL)animated;

@end

@protocol DPTopTabsViewControllerDelegate <NSObject>

@optional

-(BOOL)topTabsViewController:(DPTopTabsViewController *)topTabsViewController shouldActivateViewController:(UIViewController *)viewController;
-(void)topTabsViewController:(DPTopTabsViewController *)topTabsViewController willActivateViewController:(UIViewController *)viewController;
-(void)topTabsViewController:(DPTopTabsViewController *)topTabsViewController didActivateViewController:(UIViewController *)viewController;

@end

@interface DPTopTabBarItem : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *title;

@end

@interface UIViewController (DPTopTabsViewController)

@property (nonatomic, strong) DPTopTabBarItem *toptapItem;

@end


@interface DPTopTabBar : UIView

@property (nonatomic, strong) DPTopTabBarItem *item;
@property (nonatomic, strong) UIColor *tint;
@property (nonatomic, strong) UIColor *selectedColor;
@property (nonatomic, strong) UIColor *notSelectedColor;
@property (nonatomic) UITextAlignment textAlignment;
@property (nonatomic) CGFloat sideMargin;
@property (nonatomic) BOOL selected;
@property (nonatomic) DPTopTabsStyle tabStyle;

-(void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state;
-(void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state;

@end
