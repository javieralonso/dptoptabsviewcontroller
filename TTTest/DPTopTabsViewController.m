//
//  DPTopTabsViewController.m
//
//  Created by Diego Peinador on 20/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import "DPTopTabsViewController.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>

#define TOPTABS_CP_HEIGHT_FACTOR 3/32
#define TOPTABS_MIN_HEIGHT_NORMAL 24
#define TOPTABS_MIN_HEIGHT_TABBAR 44
#define TOPTABS_MIN_WIDTH_TABBAR 64

@interface DPTopTabsViewController (){
    NSMutableArray *_controllers,*_titleViews;
    DPTopTabBar *_currentTitleView;
    UIView *_currentContentView;
    UIScrollView *_titleScrollView;
    BOOL _animating;
    CGFloat _titleViewminWidth;
    BOOL _titleOverlapCustomized;
    UIImage *_backgroungImageNormal;
    UIImage *_backgroungImageSelected;
    UIImage *_dividerImage1; // N - N
    UIImage *_dividerImage2; // N - S
    UIImage *_dividerImage3; // S - N
    BOOL _useBackgroundImages;
    NSMutableArray *_dividers;
    NSDictionary *_textAttributesForNormal,*_textAttributesForSelected;
}
-(void)activateViewController:(UIViewController *)viewController animated:(BOOL)animated;
-(void)tappedTitleView:(UITapGestureRecognizer *)tapGR;
-(void)updateTitleSizes;
-(void)updateDividerImages;
@end

@implementation DPTopTabsViewController

@synthesize delegate=_delegate;
@synthesize tint=_tint;
@synthesize titlesBackgroundColor=_titlesBackgroundColor;
@synthesize selectedColor=_selectedColor;
@synthesize notSelectedColor=_notSelectedColor;
@synthesize textAlignment=_textAlignment;
@synthesize titleViewSize=_titleViewSize;
@synthesize titleOverlap=_titleOverlap;
@synthesize activeTab = _activeTab;
@synthesize tabStyle=_tabStyle;

#pragma mark - Customized setters

-(void)setTint:(UIColor *)tint{
    if (tint!=_tint) {
        _tint=tint;
        if ([self isViewLoaded]) {
            for (DPTopTabBar *bar in _titleViews) {
                bar.tint=_tint;
            }
        }
    }
}

-(void)setSelectedColor:(UIColor *)selectedColor{
    if (selectedColor!=_selectedColor) {
        _selectedColor=selectedColor;
        if ([self isViewLoaded]) {
            for (DPTopTabBar *bar in _titleViews) {
                bar.selectedColor=_selectedColor;
            }
        }
    }
}

-(void)setNotSelectedColor:(UIColor *)notSelectedColor{
    if (notSelectedColor!=_notSelectedColor) {
        _notSelectedColor=notSelectedColor;
        if ([self isViewLoaded]) {
            for (DPTopTabBar *bar in _titleViews) {
                bar.notSelectedColor=_notSelectedColor;
            }
        }
    }
}

-(void)setTitlesBackgroundColor:(UIColor *)titlesBackgroundColor{
    if (_titlesBackgroundColor != titlesBackgroundColor) {
        _titlesBackgroundColor = titlesBackgroundColor;
        _titleScrollView.backgroundColor = _titlesBackgroundColor;
        for (int i=0; i<[_titleViews count]; i++) {
            [[_titleViews objectAtIndex:i] setNeedsDisplay];
        }
    }
}

-(void)setTextAlignment:(UITextAlignment)textAlignment{
    if (_textAlignment != textAlignment) {
        _textAlignment = textAlignment;
        if ([self isViewLoaded]) {
            for (DPTopTabBar *bar in _titleViews) {
                bar.textAlignment=_textAlignment;
            }
        }
    }
}

-(void)setTabStyle:(DPTopTabsStyle)tabStyle{
    if (_tabStyle != tabStyle) {
        _tabStyle = tabStyle;
        if (_tabStyle==DPTopTabsStyleLeftTabBar|_tabStyle==DPTopTabsStyleRightTabBar){
            _titleOverlap = 0;
        }
        if ([self isViewLoaded]) {
            for (DPTopTabBar *bar in _titleViews) {
                bar.tabStyle=_tabStyle;
            }
            CGFloat tempSize=_titleViewSize;
            _titleViewSize = 0;
            self.titleViewSize = tempSize; // To update size so it is at least the minimum
        }
    }
}

-(void)setTitleViewSize:(CGFloat)titleViewHeight{
    if (!_useBackgroundImages && titleViewHeight!=_titleViewSize) {
        _titleViewSize=titleViewHeight;
        CGFloat minSize;
        if (_tabStyle==DPTopTabsStyleNormal) {
            minSize = TOPTABS_MIN_HEIGHT_NORMAL;
        }else if (_tabStyle==DPTopTabsStyleLikeTabBar){
            minSize = TOPTABS_MIN_HEIGHT_TABBAR;
            _titleViewminWidth = minSize*2.0;
        }else{
            minSize = TOPTABS_MIN_WIDTH_TABBAR;
            _titleViewminWidth = minSize;
        }
        if (_titleViewSize<minSize) {
            _titleViewSize=minSize;
        }
        if (!_titleOverlapCustomized && (_tabStyle==DPTopTabsStyleNormal|_tabStyle==DPTopTabsStyleLikeTabBar)) {
            _titleOverlap = _titleViewSize/2.0;
            for (DPTopTabBar *bar in _titleViews) {
                bar.sideMargin=_titleOverlap;
            }
        }
        if (_tabStyle==DPTopTabsStyleNormal){
            _titleViewminWidth=_titleViewSize+TOPTABS_TITLE_IMAGE_SIDE+TOPTABS_TITLE_MIN_WIDTH+_titleOverlap;
        }
        if ([self isViewLoaded]) {
            [self updatePageLayout];
        }
    }
}

-(void)setTitleOverlap:(CGFloat)titleOverlap{
    if (!_useBackgroundImages && titleOverlap!=_titleOverlap) {
        _titleOverlap = titleOverlap;
        if ([self isViewLoaded]) {
            [self updateTitleSizes];
        }
    }
    _titleOverlapCustomized = YES;
}

-(void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state{
    _useBackgroundImages = YES;
    switch (state) {
        case UIControlStateNormal:
            _backgroungImageNormal = backgroundImage;
            break;
        case UIControlStateSelected:
            _backgroungImageSelected = backgroundImage;
            break;
            
        default:
            break;
    }
    _titleOverlap=0;
    if (_tabStyle==DPTopTabsStyleNormal) {
        _titleViewSize = backgroundImage.size.height;
        _titleViewminWidth=_titleViewSize+TOPTABS_TITLE_IMAGE_SIDE+TOPTABS_TITLE_MIN_WIDTH;
    }
    if (_tabStyle==DPTopTabsStyleLikeTabBar) {
        _titleViewSize = backgroundImage.size.height;
    }
    if ([self isViewLoaded]) {
        for (DPTopTabBar *bar in _titleViews) {
            [bar setBackgroundImage:backgroundImage forState:state];
        }
        [self updatePageLayout];
    }
}

-(void)setDividerImage:(UIImage *)dividerImage forLeftSegmentState:(UIControlState)leftState rightSegmentState:(UIControlState)rightState{
    _useBackgroundImages = YES;
    if (leftState==UIControlStateNormal && rightState==UIControlStateNormal) {
        _dividerImage1 = dividerImage;
    } else if (leftState==UIControlStateNormal && rightState==UIControlStateSelected) {
        _dividerImage2 = dividerImage;
    } else if (leftState==UIControlStateSelected && rightState==UIControlStateNormal) {
        _dividerImage3 = dividerImage;
    }
    [self updateDividerImages];
}

-(void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state{
    if (state==UIControlStateNormal) {
        _textAttributesForNormal=attributes;
    }else if (state==UIControlStateSelected){
        _textAttributesForSelected=attributes;
    }else{
        return;
    }
    if ([self isViewLoaded]) {
        for (DPTopTabBar *bar in _titleViews) {
            [bar setTitleTextAttributes:attributes forState:state];
        }
    }
}


#pragma mark - Initialization

-(void)commonInitialization{
    _activeTab=0;
    _animating=NO;
    _titleOverlapCustomized = NO;
    _useBackgroundImages = NO;
    _tabStyle=DPTopTabsStyleNormal;
    _titleViewSize=2*TOPTABS_TITLE_TOP_MARGIN+TOPTABS_TITLE_IMAGE_SIDE;
    _titleOverlap = _titleViewSize/2;
    _titleViewminWidth=_titleViewSize+TOPTABS_TITLE_IMAGE_SIDE+TOPTABS_TITLE_MIN_WIDTH+_titleOverlap;
    _controllers=[NSMutableArray array];
    _titleViews=[NSMutableArray array];
    _dividers=[NSMutableArray array];
    _tint=[UIColor blueColor];
    _titlesBackgroundColor = [UIColor blackColor];
}

-(id)init{
    self = [super init];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

-(id)initWithViewControllers:(UIViewController *)firstVC, ...{
    self=[super init];
    if (self) {
        [self commonInitialization];
        va_list args;
        va_start(args, firstVC);
        for (id arg = firstVC; arg != nil; arg = va_arg(args, UIViewController*))
        {
            if ([arg isKindOfClass:[UIViewController class]]) {
                [_controllers addObject:arg];
                [self addChildViewController:arg];
                [(UIViewController *)arg didMoveToParentViewController:self];
            }
        }
        va_end(args);
        self.title=((UIViewController*)[_controllers objectAtIndex:_activeTab]).title;
    }
    return self;
}


#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    self.view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, _titleViewSize + 100)];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.view.backgroundColor=[UIColor clearColor];
    self.view.clipsToBounds=YES;
    [self.view addObserver:self forKeyPath:@"frame" options:0 context:NULL];

    _titleScrollView=[[UIScrollView alloc] init];
    _titleScrollView.showsHorizontalScrollIndicator = NO;
    _titleScrollView.showsVerticalScrollIndicator = NO;
    _titleScrollView.backgroundColor = _titlesBackgroundColor;
    [self.view addSubview:_titleScrollView];
    
    [self updatePageLayout];
    
    if ([_controllers count]>0) {
        [self updateTitleContentSize];
        for (int i=0; i<[_controllers count]; i++) {
            DPTopTabBar *titleView=[self toptabBarElementForIndex:i];
            if (_backgroungImageNormal) {
                [titleView setBackgroundImage:_backgroungImageNormal forState:UIControlStateNormal];
            }
            if (_backgroungImageSelected) {
                [titleView setBackgroundImage:_backgroungImageSelected forState:UIControlStateSelected];
            }
            if (!_useBackgroundImages) {
                titleView.sideMargin=_titleOverlap;
            }
            if (_textAttributesForSelected) {
                [titleView setTitleTextAttributes:_textAttributesForSelected forState:UIControlStateSelected];
            }
            if (_textAttributesForNormal) {
                [titleView setTitleTextAttributes:_textAttributesForNormal forState:UIControlStateNormal];
            }
            titleView.tabStyle = _tabStyle;
            titleView.tint=_tint;
            titleView.selectedColor=_selectedColor;
            titleView.notSelectedColor=_notSelectedColor;
            titleView.textAlignment=_textAlignment;
            titleView.backgroundColor=[UIColor clearColor];
            UIViewController *uiVC = [_controllers objectAtIndex:i];
            DPTopTabBarItem *item;
            if (uiVC.toptapItem) {
                item = uiVC.toptapItem;
            }else{
                item=[[DPTopTabBarItem alloc] init];
                item.title=uiVC.title;
                item.image=nil;
            }
            titleView.item=item;
            if (i<=_activeTab) {
                [_titleScrollView addSubview:titleView];
                _currentTitleView = titleView;
            }else{
                [_titleScrollView insertSubview:titleView belowSubview:_currentTitleView];
            }
        }
        [ self updateDividerImages];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([_controllers count]>0) {
        UIView *nextView = [[_controllers objectAtIndex:0] view];
        nextView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:nextView];
        _currentContentView = nextView;
        [self updatePageLayout];
        DPTopTabBar *titleView = [_titleViews objectAtIndex:0];
        titleView.selected = YES;
    }
}

-(void)viewWillUnload{
    [super viewWillUnload];
    [self.view removeObserver:self forKeyPath:@"frame"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [_titleViews removeAllObjects];
    _currentTitleView=nil;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    for (int i=0; i<[_titleViews count]; i++) {
        [[_titleViews objectAtIndex:i] setNeedsDisplay];
    }
}

-(void)viewDidLayoutSubviews{
    [self updateDividerImages];
}


#pragma mark - ViewControllers lifecycle

-(int)viewControllersCount{
    return [_controllers count];
}

-(void)addViewController:(UIViewController *)newVC animated:(BOOL)animated{
    [self insertViewController:newVC atIndex:[_controllers count] animated:animated];
}

-(void)insertViewController:(UIViewController *)newVC atIndex:(int)index animated:(BOOL)animated{
    if (index<=[_controllers count]) {
        if (_controllers.count==0) {
            self.title=newVC.title;
        }
        [_controllers insertObject:newVC atIndex:index];
        [self addChildViewController:newVC];
        if ([self isViewLoaded]) {
            if (index<_activeTab) {
                _activeTab++;
            }
            
            [self updateTitleContentSize];
            
            DPTopTabBar *titleView=[self toptabBarElementForIndex:index];
            if (_backgroungImageNormal) {
                [titleView setBackgroundImage:_backgroungImageNormal forState:UIControlStateNormal];
            }
            if (_backgroungImageSelected) {
                [titleView setBackgroundImage:_backgroungImageSelected forState:UIControlStateSelected];
            }
            if (!_useBackgroundImages) {
                titleView.sideMargin=_titleOverlap;
            }
            if (_textAttributesForSelected) {
                [titleView setTitleTextAttributes:_textAttributesForSelected forState:UIControlStateSelected];
            }
            if (_textAttributesForNormal) {
                [titleView setTitleTextAttributes:_textAttributesForNormal forState:UIControlStateNormal];
            }
            titleView.tabStyle = _tabStyle;
            titleView.tint=_tint;
            titleView.selectedColor=_selectedColor;
            titleView.notSelectedColor=_notSelectedColor;
            titleView.textAlignment=_textAlignment;
            titleView.backgroundColor=[UIColor clearColor];
            DPTopTabBarItem *item;
            if (newVC.toptapItem) {
                item = newVC.toptapItem;
            }else{
                item=[[DPTopTabBarItem alloc] init];
                item.title=newVC.title;
                item.image=nil;
            }
            titleView.item=item;
            [_titleScrollView insertSubview:titleView belowSubview:_currentTitleView];
            
            [self.view bringSubviewToFront:_currentContentView];
            animated = _useBackgroundImages?NO:animated;
            _animating=YES;
            [UIView animateWithDuration:animated?TOPTABS_ANIMATION_DURATION:0 animations:^(){
                [self updateTitleSizes];
            } completion:^(BOOL finished){
                _animating=NO;
                for (int i=0; i<[_titleViews count]; i++) {
                    [[_titleViews objectAtIndex:i] setNeedsDisplay];
                }
                [ self updateDividerImages];
                [newVC didMoveToParentViewController:self];
            }];
        }else{
            [newVC didMoveToParentViewController:self];
        }
    }
}

-(void)removeViewControllerAtIndex:(int)index animated:(BOOL)animated{
    if (index!=_activeTab && index<[_controllers count]) {
        if (index<_activeTab) {
            _activeTab--;
        }
        UIViewController *removingVC=[_controllers objectAtIndex:index];
        [_controllers removeObjectAtIndex:index];
        [removingVC willMoveToParentViewController:nil];
        if ([self isViewLoaded]) {
            UIView *titleView=[_titleViews objectAtIndex:index];
            [_titleViews removeObjectAtIndex:index];

            [self updateTitleContentSize];
            animated = _useBackgroundImages?NO:animated;
            _animating=YES;
            [UIView animateWithDuration:animated?TOPTABS_ANIMATION_DURATION:0 animations:^(){
                [self updateTitleSizes];
            } completion:^(BOOL finished){
                _animating=NO;
                [titleView removeFromSuperview];
                for (int i=0; i<[_titleViews count]; i++) {
                    [[_titleViews objectAtIndex:i] setNeedsDisplay];
                }
                [ self updateDividerImages];
                [removingVC removeFromParentViewController];
            }];
            
        }else{
            [removingVC removeFromParentViewController];
        }
    }else{
        //maybe some day
    }
}

-(UIViewController *)viewControllerAtIndex:(int)index{
    if (index<0 || index>=[_controllers count]) {
        return nil;
    }
    return [_controllers objectAtIndex:index];
}

-(void)activateViewControllerAtIndex:(int)index animated:(BOOL)animated{
    if (index>=0 && index<[_controllers count] && index!=_activeTab && !_animating &&
        (![self.delegate respondsToSelector:@selector(topTabsViewController:shouldActivateViewController:)] ||
         ([self.delegate respondsToSelector:@selector(topTabsViewController:shouldActivateViewController:)] && [self.delegate topTabsViewController:self shouldActivateViewController:[_controllers objectAtIndex:index]]))) {
            self.title=((UIViewController*)[_controllers objectAtIndex:index]).title;
            if ([self isViewLoaded]) {
                //activar el tab
                UIView *nextView=[[_controllers objectAtIndex:index] view];
                if ([self.delegate respondsToSelector:@selector(topTabsViewController:willActivateViewController:)]) {
                    [self.delegate topTabsViewController:self willActivateViewController:[_controllers objectAtIndex:index]];
                }
                nextView.frame=_currentContentView.frame;
                nextView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                nextView.alpha = 0.1;
                
                DPTopTabBar *tempView;
                for (int i=0; i<[_titleViews count]; i++) {
                    tempView = [_titleViews objectAtIndex:i];
                    [_titleScrollView bringSubviewToFront:tempView];
                    tempView.selected=NO;
                }
                tempView = [_titleViews objectAtIndex:index];
                tempView.selected = YES;
                [_titleScrollView bringSubviewToFront:tempView];

                NSInteger previous = _activeTab;
                _activeTab=index;
                [ self updateDividerImages];
                _animating=YES;
                [self transitionFromViewController:[_controllers objectAtIndex:previous] toViewController:[_controllers objectAtIndex:index] duration:animated?TOPTABS_ANIMATION_DURATION:0 options:0 animations:^(){
                    _currentContentView.layer.shouldRasterize=YES;
                    nextView.layer.shouldRasterize=YES;
                    _currentContentView.alpha = 0.1;
                    nextView.alpha = 1.0;
                } completion:^(BOOL finished){
                    _animating=NO;
                    _currentContentView.layer.shouldRasterize=NO;
                    nextView.layer.shouldRasterize=NO;
                    [_currentContentView removeFromSuperview];
                    _currentContentView.alpha = 1;
                    if ([self.delegate respondsToSelector:@selector(topTabsViewController:didActivateViewController:)]) {
                        [self.delegate topTabsViewController:self didActivateViewController:[_controllers objectAtIndex:index]];
                    }
                    _currentContentView=nextView;
                }];
            }else{
                [self transitionFromViewController:[_controllers objectAtIndex:_activeTab] toViewController:[_controllers objectAtIndex:index] duration:0 options:0 animations:NULL completion:NULL];
                _activeTab=index;
            }
        }
}

-(void)activateViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [self activateViewControllerAtIndex:[_controllers indexOfObject:viewController] animated:animated];
}


#pragma mark - Support methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"frame"]){
        [self updateTitleSizes];
    }
}

-(void)updatePageLayout{
    if (_tabStyle==DPTopTabsStyleNormal | _tabStyle==DPTopTabsStyleLikeTabBar) {
        _titleScrollView.frame = CGRectMake(0, 0, self.view.bounds.size.width, _titleViewSize);
        _titleScrollView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
        _currentContentView.frame=CGRectMake(0, _titleViewSize, self.view.bounds.size.width, self.view.bounds.size.height-_titleViewSize);
    }else if (_tabStyle==DPTopTabsStyleLeftTabBar){
        _titleScrollView.frame = CGRectMake(0, 0, _titleViewSize, self.view.bounds.size.height);
        _titleScrollView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin;
        _currentContentView.frame=CGRectMake(_titleViewSize, 0, self.view.bounds.size.width-_titleViewSize, self.view.bounds.size.height);
    }else{
        _titleScrollView.frame = CGRectMake(self.view.bounds.size.width-_titleViewSize, 0, _titleViewSize, self.view.bounds.size.height);
        _titleScrollView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin;
        _currentContentView.frame=CGRectMake(0, 0, self.view.bounds.size.width-_titleViewSize, self.view.bounds.size.height);
    }
    [self updateTitleSizes];
}

-(void)updateTitleContentSize{
    if (_tabStyle==DPTopTabsStyleNormal | _tabStyle==DPTopTabsStyleLikeTabBar) {
        CGFloat titleWidth = (self.view.bounds.size.width+_titleOverlap*([_controllers count]-1)) / [_controllers count];
        if (titleWidth<_titleViewminWidth) {
            titleWidth = _titleViewminWidth;
        }
        _titleScrollView.contentSize = CGSizeMake((titleWidth-_titleOverlap)*([_controllers count]-1)+titleWidth, _titleViewSize);
    }else{
        _titleScrollView.contentSize = CGSizeMake(_titleViewSize, [_controllers count]*_titleViewSize);
    }
}

-(void)updateTitleSizes{
    if (_tabStyle==DPTopTabsStyleNormal | _tabStyle==DPTopTabsStyleLikeTabBar) {
        CGFloat titleWidth = (self.view.bounds.size.width+_titleOverlap*([_controllers count]-1)) / [_controllers count];
        if (titleWidth<_titleViewminWidth) {
            titleWidth = _titleViewminWidth;
        }
        _titleScrollView.contentSize = CGSizeMake((titleWidth-_titleOverlap)*([_controllers count]-1)+titleWidth, _titleViewSize);
        UIView *tempView;
        for (int i=0; i<[_titleViews count]; i++) {
            tempView = [_titleViews objectAtIndex:i];
            tempView.frame = CGRectMake(i*(titleWidth-_titleOverlap), 0, titleWidth, _titleViewSize);
            [tempView setNeedsDisplay];
        }
    }else{
        _titleScrollView.contentSize = CGSizeMake(_titleViewSize, [_controllers count]*_titleViewSize);
        UIView *tempView;
        for (int i=0; i<[_titleViews count]; i++) {
            tempView = [_titleViews objectAtIndex:i];
            tempView.frame = CGRectMake(0, i*_titleViewSize, _titleViewSize, _titleViewSize);
            [tempView setNeedsDisplay];
        }
    }
}

-(void)updateDividerImages{
    if (_useBackgroundImages && [_controllers count]) {
        UIImageView *tempDivider;
        while ([_dividers count]>=[_controllers count]) {
            tempDivider = [_dividers lastObject];
            [_dividers removeLastObject];
            [tempDivider removeFromSuperview];
        }
        while ([_dividers count]<([_controllers count]-1)) {
            tempDivider = [[UIImageView alloc] initWithImage:_dividerImage1];
            [_dividers addObject:tempDivider];
            [_titleScrollView addSubview:tempDivider];
        }
        CGFloat titleWidth = _titleScrollView.contentSize.width / [_controllers count];
        for (int i=0; i<[_dividers count]; i++) {
            tempDivider = [_dividers objectAtIndex:i];
            tempDivider.image = _dividerImage1;
            if (_tabStyle==DPTopTabsStyleNormal | _tabStyle==DPTopTabsStyleLikeTabBar) {
                tempDivider.frame = CGRectMake(titleWidth*(i+1)-tempDivider.image.size.width/2, 0, tempDivider.image.size.width, tempDivider.image.size.height);
            }else{
                tempDivider.frame = CGRectMake(0, _titleViewSize*(i+1)-tempDivider.image.size.height/2, tempDivider.image.size.width, tempDivider.image.size.height);
            }
            [_titleScrollView bringSubviewToFront:tempDivider];
        }
        if (_activeTab>0 && _dividerImage2) {
            tempDivider = [_dividers objectAtIndex:_activeTab-1];
            tempDivider.image = _dividerImage2;
        }
        if (_activeTab<([_controllers count]-1) && _dividerImage3) {
            tempDivider = [_dividers objectAtIndex:_activeTab];
            tempDivider.image = _dividerImage3;
        }
    }
}

-(void)tappedTitleView:(UITapGestureRecognizer *)tapGR{
    [self activateViewControllerAtIndex:[_titleViews indexOfObject:tapGR.view] animated:YES];
}

-(DPTopTabBar *)toptabBarElementForIndex:(NSUInteger)index{
    DPTopTabBar *result=nil;
    
    CGFloat titleWidth = (self.view.bounds.size.width+_titleOverlap*([_controllers count]-1)) / [_controllers count];
    if (titleWidth<_titleViewminWidth) {
        titleWidth = _titleViewminWidth;
    }

    result = [[DPTopTabBar alloc] initWithFrame:CGRectMake(index*(titleWidth-_titleOverlap), _titleViewSize, titleWidth, _titleViewSize)];
    result.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedTitleView:)];
    [result addGestureRecognizer:tapGR];
    [_titleViews insertObject:result atIndex:index];

    
    return result;
}


#pragma mark - Memory management

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc{
    if (self.isViewLoaded) {
        [self.view removeObserver:self forKeyPath:@"frame"];
    }
}

@end

#pragma mark -

@implementation DPTopTabBarItem

@synthesize image=_image;
@synthesize title=_title;

@end

#pragma mark -

@interface DPTopTabBar(){
    UILabel *_titleLabel;
    UIImageView *_imageView;
    UIImage *_backgroungImageNormal;
    UIImage *_backgroungImageSelected;
    UIImageView *_backgroundImageView;
    NSDictionary *_textAttributesForNormal,*_textAttributesForSelected;
}
-(void)updateImage;
-(void)updateLabelAttributes;
@end

@implementation DPTopTabBar

@synthesize item=_item;
@synthesize tint=_tint;
@synthesize selected=_selected;
@synthesize selectedColor=_selectedColor;
@synthesize notSelectedColor=_notSelectedColor;
@synthesize textAlignment=_textAlignment;
@synthesize sideMargin=_sideMargin;
@synthesize tabStyle=_tabStyle;

#pragma mark - Initialization

-(id)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        _tint=[UIColor blueColor];
        _selectedColor = nil;
        _notSelectedColor = nil;
        _sideMargin = self.bounds.size.height/2;
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.text=@"";
        _titleLabel.textColor=[UIColor whiteColor];
        _titleLabel.shadowColor=[UIColor darkGrayColor];
        _titleLabel.shadowOffset=CGSizeMake(1, 1);
        _titleLabel.backgroundColor=[UIColor clearColor];
        _titleLabel.font=[UIFont boldSystemFontOfSize:[UIFont labelFontSize]];
        _titleLabel.userInteractionEnabled=NO;
        [self addSubview:_titleLabel];
        _imageView=[[UIImageView alloc] init];
        _imageView.opaque=NO;
        _imageView.backgroundColor=[UIColor clearColor];
        _imageView.userInteractionEnabled=NO;
        [self addSubview:_imageView];
        _selected=NO;
    }
    
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat sideMarginLeft = _sideMargin;
    CGFloat sideMarginRight = _sideMargin;
    if (_backgroundImageView) {
        UIImage *backgroungImage;
        if (_selected && _backgroungImageSelected) {
            backgroungImage = _backgroungImageSelected;
        }else{
            backgroungImage = _backgroungImageNormal;
        }
        sideMarginLeft = backgroungImage.capInsets.left;
        sideMarginRight = backgroungImage.capInsets.right;
    }
    if (_tabStyle==DPTopTabsStyleNormal) {
        CGRect titleLabelFrame;
        if (!_item.image) {
            titleLabelFrame = CGRectMake(sideMarginLeft, 0, self.bounds.size.width -sideMarginLeft -sideMarginRight, self.bounds.size.height);
        }else{
            titleLabelFrame = CGRectMake(sideMarginLeft + TOPTABS_TITLE_IMAGE_SIDE+2, 0, self.bounds.size.width - sideMarginLeft - sideMarginRight - TOPTABS_TITLE_IMAGE_SIDE-2, self.bounds.size.height);
        }

        _titleLabel.frame = titleLabelFrame;
        _titleLabel.textAlignment=_textAlignment;
        _titleLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

        _imageView.frame = CGRectMake(_sideMargin, (self.bounds.size.height-TOPTABS_TITLE_IMAGE_SIDE)/2, TOPTABS_TITLE_IMAGE_SIDE, TOPTABS_TITLE_IMAGE_SIDE);
        _imageView.contentMode=UIViewContentModeScaleAspectFit;
        _imageView.autoresizingMask=UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
    }else{
        _titleLabel.frame = CGRectMake(0, self.bounds.size.height-21, self.bounds.size.width, 19);
        _titleLabel.textAlignment=NSTextAlignmentCenter;
        _titleLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
        
        _imageView.frame = CGRectMake(0, 4, self.bounds.size.width, self.bounds.size.height-27);
        _imageView.contentMode=UIViewContentModeScaleAspectFit;
        _imageView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    }
}

#pragma mark - Customized setters

-(void)setTint:(UIColor *)tint{
    if (tint!=_tint) {
        _tint=tint;
        [self setNeedsDisplay];
    }
}

-(void)setSelectedColor:(UIColor *)selectedColor{
    if (selectedColor!=_selectedColor) {
        _selectedColor=selectedColor;
        [self setNeedsDisplay];
    }
}

-(void)setNotSelectedColor:(UIColor *)notSelectedColor{
    if (notSelectedColor!=_notSelectedColor) {
        _notSelectedColor=notSelectedColor;
        [self setNeedsDisplay];
    }
}

-(void)setSelected:(BOOL)selected{
    if (selected!=_selected) {
        _selected=selected;
        [self updateLabelAttributes];
        [self setNeedsDisplay];
    }
}

-(void)setTextAlignment:(UITextAlignment)textAlignment{
    if (textAlignment!=_textAlignment) {
        _textAlignment=textAlignment;
        if (_tabStyle==DPTopTabsStyleNormal) {
            _titleLabel.textAlignment=_textAlignment;
        }
    }
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    [self updateImage];
    _backgroundImageView.frame = self.bounds;
}

-(void)setItem:(DPTopTabBarItem *)item{
    if (item!=_item) {
        [_item removeObserver:self forKeyPath:@"title"];
        [_item removeObserver:self forKeyPath:@"image"];
        _item=item;
        _titleLabel.text=_item.title;
        [self updateImage];
        [_item addObserver:self forKeyPath:@"image" options:0 context:NULL];
        [_item addObserver:self forKeyPath:@"title" options:0 context:NULL];
    }
}

-(void)setBackgroundImage:(UIImage *)backgroundImage forState:(UIControlState)state{
    switch (state) {
        case UIControlStateNormal:
            _backgroungImageNormal = backgroundImage;
            break;
        case UIControlStateSelected:
            _backgroungImageSelected = backgroundImage;
            break;
            
        default:
            break;
    }
    if (_backgroungImageNormal && !_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_backgroundImageView];
        [self sendSubviewToBack:_backgroundImageView];
        [self setNeedsLayout];
    }
}

-(void)setTitleTextAttributes:(NSDictionary *)attributes forState:(UIControlState)state{
    if (state==UIControlStateSelected) {
        _textAttributesForSelected=attributes;
    }else if (state==UIControlStateNormal){
        _textAttributesForNormal=attributes;
    }else{
        return;
    }
    [self updateLabelAttributes];
}

-(void)updateLabelAttributes{
    NSDictionary *attributes;
    if (_selected && _textAttributesForSelected) {
        attributes=_textAttributesForSelected;
    }else if (_textAttributesForNormal){
        attributes=_textAttributesForNormal;
    }else{
        return;
    }
    if ([attributes objectForKey:UITextAttributeFont]) {
        _titleLabel.font=[attributes objectForKey:UITextAttributeFont];
    }
    if ([attributes objectForKey:UITextAttributeTextColor]) {
        _titleLabel.textColor=[attributes objectForKey:UITextAttributeTextColor];
    }
    if ([attributes objectForKey:UITextAttributeTextShadowColor]) {
        _titleLabel.shadowColor=[attributes objectForKey:UITextAttributeTextShadowColor];
    }
    if ([attributes objectForKey:UITextAttributeTextShadowOffset]) {
#warning shadowOffset is a CGSize and UITextAttributeTextShadowOffset is an UIOffset. Apple what are you playing?
        _titleLabel.shadowOffset=[[attributes objectForKey:UITextAttributeTextShadowOffset] CGSizeValue];
    }
}


#pragma mark - Support methods

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object==_item) {
        if ([keyPath isEqualToString:@"title"]) {
            _titleLabel.text=_item.title;
        }else if ([keyPath isEqualToString:@"image"]){
            [self updateImage];
        }
    }
}

-(void)updateImage{
    _imageView.image=_item.image;
    [self setNeedsLayout];
}

#pragma mark - Draw Tab

-(void)drawRect:(CGRect)rect{
    
    if (_backgroundImageView) {
        if (_selected && _backgroungImageSelected) {
            _backgroundImageView.image = _backgroungImageSelected;
        }else{
            _backgroundImageView.image = _backgroungImageNormal;
        }
    }else{
        CGFloat hue, saturation, brightness;
        [_tint getHue:&hue saturation:&saturation brightness:&brightness alpha:NULL];
        
        self.backgroundColor = [UIColor clearColor];
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(context);
        if (_selected) {
            if (_selectedColor) {
                [_selectedColor setFill];
            }else{
                [[UIColor colorWithHue:hue saturation:saturation*0.55 brightness:brightness alpha:1.0] setFill];
            }
        }else{
            if (_notSelectedColor) {
                [_notSelectedColor setFill];
            }else{
                [[UIColor colorWithHue:hue saturation:saturation*0.25 brightness:brightness alpha:1.0] setFill];
            }
        }
        
        UIBezierPath *path = [[UIBezierPath alloc] init];
        CGFloat height = self.bounds.size.height;
        CGFloat width = self.bounds.size.width;
        if (_tabStyle==DPTopTabsStyleNormal|_tabStyle==DPTopTabsStyleLikeTabBar) {
            [path moveToPoint:CGPointMake(0, height-TOPTABS_TITLE_LINE_WIDTH/2)];
            [path addQuadCurveToPoint:CGPointMake(height/6, height-height/6) controlPoint:CGPointMake(height*TOPTABS_CP_HEIGHT_FACTOR, height-TOPTABS_TITLE_LINE_WIDTH/2)];
            [path addLineToPoint:CGPointMake(height/2-height/6, height/6)];
            [path addQuadCurveToPoint:CGPointMake(height/2, TOPTABS_TITLE_LINE_WIDTH) controlPoint:CGPointMake(height/2-height*TOPTABS_CP_HEIGHT_FACTOR, TOPTABS_TITLE_LINE_WIDTH)];
            [path addLineToPoint:CGPointMake(width-height/2, TOPTABS_TITLE_LINE_WIDTH)];
            [path addQuadCurveToPoint:CGPointMake(width-height/2+height/6, height/6) controlPoint:CGPointMake(width-height/2+height*TOPTABS_CP_HEIGHT_FACTOR, TOPTABS_TITLE_LINE_WIDTH)];
            [path addLineToPoint:CGPointMake(width-height/6, height-height/6)];
            [path addQuadCurveToPoint:CGPointMake(width, height-TOPTABS_TITLE_LINE_WIDTH/2) controlPoint:CGPointMake(width-height*TOPTABS_CP_HEIGHT_FACTOR, height-TOPTABS_TITLE_LINE_WIDTH/2)];
            if (_selected) {
                [path addLineToPoint:CGPointMake(width, height+TOPTABS_TITLE_LINE_WIDTH)];
                [path addLineToPoint:CGPointMake(0, height+TOPTABS_TITLE_LINE_WIDTH)];
            }
            [path closePath];
        }else{
            [path moveToPoint:CGPointMake(width-width/6, TOPTABS_TITLE_LINE_WIDTH*2)];
            [path addQuadCurveToPoint:CGPointMake(width-TOPTABS_TITLE_LINE_WIDTH*2, height/6) controlPoint:CGPointMake(width-TOPTABS_TITLE_LINE_WIDTH*2, TOPTABS_TITLE_LINE_WIDTH*2)];
            [path addLineToPoint:CGPointMake(width-TOPTABS_TITLE_LINE_WIDTH*2, height-height/6)];
            [path addQuadCurveToPoint:CGPointMake(width-width/6, height-TOPTABS_TITLE_LINE_WIDTH*2) controlPoint:CGPointMake(width-TOPTABS_TITLE_LINE_WIDTH*2, height-TOPTABS_TITLE_LINE_WIDTH*2)];
            [path addLineToPoint:CGPointMake(width/6, height-TOPTABS_TITLE_LINE_WIDTH*2)];
            [path addQuadCurveToPoint:CGPointMake(TOPTABS_TITLE_LINE_WIDTH*2, height-height/6) controlPoint:CGPointMake(TOPTABS_TITLE_LINE_WIDTH*2, height-TOPTABS_TITLE_LINE_WIDTH*2)];
            [path addLineToPoint:CGPointMake(TOPTABS_TITLE_LINE_WIDTH*2, height/6)];
            [path addQuadCurveToPoint:CGPointMake(width/6, TOPTABS_TITLE_LINE_WIDTH*2) controlPoint:CGPointMake(TOPTABS_TITLE_LINE_WIDTH*2, TOPTABS_TITLE_LINE_WIDTH*2)];
            [path closePath];
        }
        [_tint setStroke];
        path.lineWidth = TOPTABS_TITLE_LINE_WIDTH;
        [path fill];
        [path stroke];
        [path addClip];
        
        CGContextRestoreGState(context);
    }
}

-(void)setNeedsDisplay{
    [super setNeedsDisplay];
}

#pragma mark - Memory management

-(void)dealloc{
    [_item removeObserver:self forKeyPath:@"title"];
    [_item removeObserver:self forKeyPath:@"image"];
}

@end


#pragma mark -

@implementation UIViewController (DPTopTabsViewController)

@dynamic toptapItem;

static char const * const toptapItemKey = "toptapItem";

- (id)toptapItem {
    return objc_getAssociatedObject(self, toptapItemKey);
}

- (void)setToptapItem:(DPTopTabBarItem*)newToptapItem {
    objc_setAssociatedObject(self, toptapItemKey, newToptapItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

